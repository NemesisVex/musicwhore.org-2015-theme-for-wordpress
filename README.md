# Musicwhore.org 2015 Theme

A custom theme for [Musicwhore.org](https://musicwhore.org/).

## Dependencies

* GruntJS

## Implementations

### Movable Type ID Mapper

`Setup::register_mt_id_patterns()` registers patterns to match content containing Movable Type URls
with entry IDs. See the
[plugin documentation](https://bitbucket.org/NemesisVex/movable-type-id-mapper-for-wordpress)
for usage.

### Bootstrap Overrides for WordPress

`TemplateTags::paging_nav()` checks whether the Bootstrap Overrides plugin is enabled. If so,
it uses `Setup::wp_page_menu_args()` to pass arguments to the overridden paging functions.
See the [plugin documentation](https://bitbucket.org/NemesisVex/bootstrap-overrides-for-wordpress)
for usage.

## Child Themes

* [Musicwhore.org Archive](https://bitbucket.org/NemesisVex/musicwhore.org-archive-theme-for-wordpress)
